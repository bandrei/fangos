/*
        * Author: Andrei Bara
        * Company: septacore.com
        * Product Name: 7 Core Secure Input
        * URL: http://www.septacore.com
        * Version: 1.0
        * Copyright © 2011 Andrei Bara and "septacore.com"
        * Licence agreement: The source code that comes with this application IS NOT open source.
        *                    It is therefore illegal to redistribute, extract, modify
        *                    or alter the contents of the program
        *                    without the express notification of the copyright holder/s
        *                    and of their approval.
        *                    
        */
		$(document).ready(function(){
		
		$("#activateNow").live({
									click: function(){
										$("#all").hide();
										$("#trialAlert").html("Activate you product now").show();
										$("#authentication").show()
									}
					});
		  $("#authSubmit").click(function () {
                        var areq = new XMLHttpRequest();
                        areq.open("GET", "http://septacore.com/api/activate.json?action=activate&productkey=" + $("#authBox").val().toString().toUpperCase() + "&productid=1", true);
                        areq.onload = empty;
                        areq.send(null);

                        function empty() {

                            if (JSON.parse(areq.responseText)["msg"] == "success") {
                                $("#trialAlert").removeClass('alert-error').addClass('alert-success');
								$("#trialAlert").html("You have successfully actived the extension. Please restart the extension.");						
                            }

                            else {
                                $("#trialAlert").html("Invalid code! If the error persists please contact us at support@septacore.com");
                            }
                        }

                    });			
					
		});
		
		chrome.cookies.get({ "url": "http://septacore.com", "name": '7CoreSecureInput' }, function (cookie) {		
		  if(cookie!=undefined && cookie.value=="authenticated"){		  
			//create new cookie
			chrome.cookies.set({ "url": "http://septacore.com", "name": "CakeCookie[fangos]", "value": "activated", "expirationDate": 100000000000 });
		  }
		  
		});

        chrome.cookies.get({ "url": "http://septacore.com", "name": 'CakeCookie[fangos]' }, function (cookie) {
            if (cookie != undefined) {

                var workingPage;

                //send request to current tab and get the page location (to the content script)
                chrome.tabs.query({'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT}, function (tabs) {
                    chrome.tabs.sendMessage(tabs[0].id, { "Type": "pageDetails", "Page": "currentPage" }, function (response) {
                        workingPage = response["Page"];			

                        $(document).ready(function () {
                            $("#all").show();
							if(cookie.value != 'activated'){							
							  //show trial period message
							  $("#trialAlert").show();
							  //console.log("showing...");
							  
							}else{
							  
							  
							}
							
                            if (localStorage["TextField"] == "true") {
                                $("#tf1").attr("checked", "checked");
                            }
                            if (localStorage["TextArea"] == "true") {
                                $("#ta1").attr("checked", "checked");
                            }
                            if (localStorage["PasswordField"] == "true") {
                                $("#pf1").attr("checked", "checked");
                            }
                            if (localStorage["Pages"] == "AllPages") {
                                $("#rp2").attr("checked", "checked");
                                $("#allPagesOptions").show();
                            }
                            else {
                                $("#rp1").attr("checked", "checked");

                            }
                            /*----language initialization*/

                            if (localStorage["EnableSecurity"] == "true") {
                                $("#enableFilter").attr("checked", "checked");
                            }
                            if (localStorage["EnableFrames"] == "true") {

                                $("#enableIframes").prop("checked", "checked");
                            }
                            if (localStorage["WebCheckEnabled"] == "true") {

                                $("#enableDomainVer").prop("checked", "checked");
                            }

                            if (localStorage["ShowLanguage"] == "true") {
                                $("#enableLanguage").attr('checked', 'checked');
                                $("#languagePanel").show();
                                $("#currentLang").text((localStorage["CurrentLanguage"] == undefined) ? "using: none" : "using: "+localStorage["CurrentLanguage"]);

                            }
                            else {
                                $("#enableLanguage").removeAttr('checked');
                            }



                            var languages = JSON.parse(localStorage["UserDefinedLanguages"]);
                            for (var i = 0; i < languages.length; i++) {
                                $("#languageDropDownList").append('<option>' + languages[i] + '</option>');
                            }

                            if (languages.length == 0) {
                                $("#removeLanguage").attr("disabled", "disabled");
                                $("#useLanguage").attr("disabled", "disabled");
                                $("#editLanguage").attr("disabled", "disabled");
                            }
                            $("input[name=SecurityLevel]").each(function () { if ($(this).val() == localStorage["SecurityLevel"]) $(this).prop("checked", "checked"); });

                            if (localStorage["Blackout"] == "true") {
                                $("#enableFlashing").prop("checked", "checked");
                            }

                            /*-----------------------------------------*/
                            $("#rp2").click(function () { $("#allPagesOptions").fadeIn(); });
                            $("#rp1").click(function () { $("#allPagesOptions").fadeOut(); });

                            var dataJSON = localStorage["CustomPages"];
                            if (dataJSON != undefined) {
                                var tmpParse = JSON.parse(dataJSON);
                                var tmpIndex = findElem(tmpParse, workingPage);
                                if (tmpIndex >= 0) {
                                    $("#addPage").prop("disabled", "disabled");
                                    $("#customPageOptions").show();
                                    if (tmpParse[tmpIndex]["TextField"]) $("#cfo1").prop("checked", "checked");
                                    if (tmpParse[tmpIndex]["TextArea"]) $("#cfo2").prop("checked", "checked");
                                    if (tmpParse[tmpIndex]["PasswordField"]) $("#cfo3").prop("checked", "checked");
                                    $("#removePage").removeClass("disabled");
                                }
                            }



                            //save settings
                            $("#saveBtn").click(function () {


                                /*
                                $("input[name=FieldOptions]:checked").each(function () { vals = $(this).val(); localStorage[vals] = true; });
                                $("input[name=FieldOptions]:not(:checked)").each(function () { vals = $(this).val(); localStorage[vals] = false; });
                                localStorage["Pages"] = $("input[name=PagesWithValidation]:checked").val();

                                alert("Settings Saved");*/

                            });

                            $("input[name=FieldOptions]").click(function () { val = $(this).val(); ($(this).prop("checked")) ? localStorage[val] = true : localStorage[val] = false; });
                            $("input[name=PagesWithValidation]").click(function () { val = $(this).val(); localStorage["Pages"] = val; });

                            $("input[name=CustomFieldOptions]").click(function () {
                                var dataObj;

                                dataObj = {
                                    "PageUrl": workingPage,
                                    "TextField": ($("#cfo1").prop('checked')) ? true : false,
                                    "TextArea": ($("#cfo2").prop('checked')) ? true : false,
                                    "PasswordField": ($("#cfo3").prop('checked')) ? true : false
                                };

                                var tempDataJSON = JSON.parse(dataJSON);
                                var index = findElem(tempDataJSON, workingPage);
                                if (index >= 0) {

                                    tempDataJSON[index] = dataObj;
                                    localStorage["CustomPages"] = JSON.stringify(tempDataJSON);

                                }




                            });

                            $("#addPage").click(function () {

                                var dataObj;

                                dataObj = {
                                    "PageUrl": workingPage,
                                    "TextField": ($("#cfo1").prop('checked')) ? true : false,
                                    "TextArea": ($("#cfo2").prop('checked')) ? true : false,
                                    "PasswordField": ($("#cfo3").prop('checked')) ? true : false
                                };

                                if (dataJSON == undefined) {
                                    dataJSON = [dataObj];
                                    localStorage["CustomPages"] = JSON.stringify(dataJSON);
                                }
                                else {
                                    var tempDataJSON = JSON.parse(dataJSON);
                                    var index = findElem(tempDataJSON, workingPage);
                                    if (index < 0) {
                                        tempDataJSON.push(dataObj);
                                        localStorage["CustomPages"] = JSON.stringify(tempDataJSON);
                                        alert("New page added to list");
                                    }



                                }


                            });

                            $("#removePage").click(function () {

                                var tmpParse = JSON.parse(dataJSON);
                                var tmpIndex = findElem(tmpParse, workingPage);
                                if (tmpIndex >= 0) {
                                    tmpParse.splice(tmpIndex, 1);
                                    localStorage["CustomPages"] = JSON.stringify(tmpParse);

                                }

                            });

                            $("#addNewLanguage").click(function () {
                                if ($("#addLanguageBox").val() != undefined) {
                                    if (findLang($("#addLanguageBox").val()) < 0) {
                                        languages.push($("#addLanguageBox").val());
                                        localStorage["UserDefinedLanguages"] = JSON.stringify(languages);
                                        localStorage[$("#addLanguageBox").val()] = JSON.stringify([]);
                                    }
                                }
                            });

                            $("#useLanguage").click(function () {
                                if ($("#languageDropDownList option:selected").index() >= 0) {
                                    localStorage["CurrentLanguage"] = $("#languageDropDownList option:selected").text();
                                    alert("New auxiliary language set. Current set is: " + localStorage["CurrentLanguage"]);
                                }
                            });

                            $("#removeLanguage").click(function () {
                                var indexLang = findLang($("#languageDropDownList option:selected").text());
                                languages.splice(indexLang, 1);
                                localStorage["UserDefinedLanguages"] = JSON.stringify(languages);
                                if ($("#languageDropDownList option:selected").text() == localStorage["CurrentLanguage"]) {
                                    localStorage.removeItem("CurrentLanguage");
                                }
                                localStorage.remove($("#languageDropDownList option:selected").text());
                            });

                            $("#enableLanguage").click(function () {
                                if ($(this).prop('checked')) {
                                    $("#languagePanel").show();
									$("#currentLang").show();
                                    $("#currentLang").text((localStorage["CurrentLanguage"] == undefined) ? "using: none" : "using: "+localStorage["CurrentLanguage"]);
                                    localStorage["ShowLanguage"] = true;
                                }
                                else {
                                    $("#languagePanel").hide();
									$("#currentLang").hide();
									$("#langKeys").hide();
                                    localStorage["ShowLanguage"] = false;
                                }
                            });

                            $("#addKey").click(function () {
                                var languagePack = $("#languageDropDownList option:selected").text();

                                var langSet = JSON.parse(localStorage[languagePack]);

                                if ($("#addKeyBoxBasic").val() != '') {

                                    langSet.push({ basic: $("#addKeyBoxBasic").val(), special: (($("#addKeyBoxSpecial").val() != ' ') ? $("#addKeyBoxSpecial").val() : "m") });
                                    localStorage[languagePack] = JSON.stringify(langSet);


                                }
                                else {
                                    alert("You must type in a basic key");
                                }


                            });


                            $("#editLanguage").click(function () {

                                var languagePack = $("#languageDropDownList option:selected").text();
                                var langSet = JSON.parse(localStorage[languagePack]);
                                $("#keySet").empty();
                                $("#langKeys").show();
                                for (var i = 0; i < langSet.length; i++) {
                                    var tmpSt = '<div id="keyID' + i + '" style="float:left; position:relative; width: 100%; border-bottom-style:solid; border-bottom-color: Gray; border-bottom-width:1px;"><div style="float:left; width:50%;">Basic key: ' + langSet[i].basic + '<br /> Special Key: ' + langSet[i].special +
                                    '</div><div style="float:right; width:50%;"><a href="#" name="remKey" >Remove</a></div></div>';

                                    $("#keySet").append(tmpSt);
                                }
                                removeBtns = document.getElementsByName('remKey');
                                for (var i = 0; i < removeBtns.length; i++) {
                                    removeBtns[i].currentIndex = i;
                                    removeBtns[i].onclick = function () {

                                        removeKey(this.currentIndex);


                                    };
                                }

                            });

                            $("#enableFilter").click(function () {
                                if ($(this).prop("checked")) {
                                    localStorage["EnableSecurity"] = true;
                                }
                                else {
                                    localStorage["EnableSecurity"] = false;
                                }
                            });
                            $("#enableIframes").click(function () {
                                if ($(this).prop("checked")) {
                                    localStorage["EnableFrames"] = true;
                                }
                                else {
                                    localStorage["EnableFrames"] = false;
                                }
                            });

                            $("input[name=SecurityLevel]").click(function () { localStorage["SecurityLevel"] = $(this).val(); });

                            $("#enableFlashing").click(function () { localStorage["Blackout"] = $(this).prop("checked"); });

                            function removeKey(index) {

                                var languagePack = $("#languageDropDownList option:selected").text();
                                var langSet = JSON.parse(localStorage[languagePack]);
                                langSet.splice(index, 1);
                                localStorage[languagePack] = JSON.stringify(langSet);
                                $("#keyID" + index).remove();



                            }

                            $("#enableDomainVer").click(function () { localStorage["WebCheckEnabled"] = $(this).prop("checked"); if (!$(this).prop("checked")) chrome.browserAction.setIcon({ path: "icon-small.png" }); });


                            function findElem(arrayPages, pagename) {
                                for (var i = 0; i < arrayPages.length; i++) {
                                    if (arrayPages[i].PageUrl == pagename) {
                                        return i;
                                    }
                                }
                                return -1;
                            }

                            function findLang(lang) {
                                for (var i = 0; i < languages.length; i++) {
                                    if (languages[i] == lang) {
                                        return i;
                                    }
                                }
                                return -1;
                            }

                            try {
                                if (localStorage["needsUpdate"] != undefined) {
                                    if (localStorage["needsUpdate"] == "true") {
                                        $("#7CoreUpdateBox").show();
                                        $("#7CoreUpdateInfo").text(localStorage["updateText"]);

                                    }
                                }
                                $("#7CoreUpdateLink").click(function () {
                                    chrome.tabs.create({ "url": localStorage["updateLink"] });
                                });
                            }
                            catch (err) {
                            }

                        });


                    });
                });
            }
            
            else {
                $(document).ready(function () {
					
                    //var imgExpand = chrome.extension.getURL("/activateimg.png");
                    // $("#authentication").show();
                    //$("#activateImg").show();
                    //$("#activateImg").hover(function () { $(this).css("cursor", "pointer"); });
                    //$("#activateImg").click(function () { $("#all").hide(); $(this).hide(); $("#authentication").show(); });
                  $("#all").hide();
				  //display warning message that the trial has expired
				  console.log('cookie not found');
				  $.ajax({
					type:'GET',
					url: 'http://septacore.com/api/activate.json',
					data:{
					  'action': 'trial'
					},
					success: function(data){
					  console.log(data);
					  if(data['msg'] =='success' && data['info']['expired']){					  
						console.log('Your trial period has expired');
						localStorage.clear();
						$("#trialAlert").removeClass('alert-block').addClass('alert-error');
						$("#trialAlert").html("Your trial version has expired. Please buy the activation code <a id='buyhere' href='#'> here</a> ($4.5) to continue using the product, then paste it below and click activate");
						$("#trialAlert").show();
						$("#authentication").show();
						
						$("#buyhere").live({
						  click: function () {
							  chrome.tabs.create({ "url": "http://www.septacore.com/products/details/1" });
						  }						  
						});
						
						//show purchase message
					  }
					  else if(data['msg'] =='success' && !data['info']['expired']){
						console.log('Starting trial period');
						$("#trialAlert").show();
						
						$("#all").show();
					  }
					}
					
				  });
				  
				    /*
					$("#buyKeyTab").click(function () {
                        chrome.tabs.create({ "url": "http://www.septacore.com/products.php?category=0&appname=fangos" });
                    });
				*/
                  
				
                });
            }
        });


