﻿/*
* Author: Andrei Bara
* Company: septacore.com
* Product Name: 7 Core Secure Input
* URL: http://www.septacore.com
* Version: 1.0
* Copyright © 2011 Andrei Bara and "septacore.com"
* Licence agreement: The source code that comes with this application IS NOT open source.
*                    It is therefore illegal to redistribute, extract, modify
*                    or alter the contents of the program
*                    without the express notification of the copyright holder/s
*                    and of their approval.
*                    
*/

function Keyboard7Core() {
    //the variable below will store the whole array of english keys
    //from a to z
    var qwertyKeyboard = new Array(27);
    //minor hack to get the ascii code of "a"
    var str = 'a';
    var char = str.charCodeAt(0);
    //---------------------------------
    var next = -1; //initial value used in the getKey function
    //for returning the next char in the list
    this.selected = "none"; //for click events
    //changed when an user click any sort of
    //editable content
    //decoy
    this.test = new Array(8);
    this.enabled = 0;
    this.upper;
    this.index = 0;
    this.currentPage; //the current operating page used in determining whether
    //the keyboard will show up or not 
    //used in conjuction with the user setting from popup.html
    this.isFrame = false;
    this.random = -1;
    this.blackout = false;

    var up = 0;
    var host = this;
    registerEvents(); //register all the events
    var el; //globalize element for subfunction access

    //register keyboard click events
    //key click, shift, enter, close button clicks
    function registerEvents() {
        $(".key7Core").live("click", function () {
            el.focus();
            var parentVal = $(this).text();
            if (host.upper == 0)
                parentVal = $(this).text().toLowerCase();
            else
                parentVal = $(this).text().toUpperCase();
            var content;

            content = el.value + parentVal;

            el.value = content.toString();
        });

        $(".doublekey7Core").live("click", function () {
            el.focus();
            var parentVal;
            if (host.upper == 0)
                parentVal = $(this).find(".basic7Core").text().toLowerCase();
            else
                parentVal = $(this).find(".special7Core").text().toLowerCase()
            var content;

            content = el.value + parentVal;

            el.value = content.toString();
        });

        $(".key7Core").live({
            mouseenter: function () { $(this).css("cursor", "pointer"); if (host.blackout) { $(".key7Core").css("background-color", "black"); $(".doublekey7Core").css("background-color", "black"); $(this).css("background-color", "White"); $(this).css("color", "white"); } else { $(this).css("background-color", "White"); } },
            mouseleave: function () { $(".key7Core").css("background-color", "#DEDEDE"); $(".key7Core").css("color", "Black"); $(".doublekey7Core").css("background-color", "#DEDEDE"); }
        });



        $(".doublekey7Core").live({
            mouseenter: function () { $(this).css("cursor", "pointer"); if (host.blackout) { $(".doublekey7Core").css("background-color", "black"); $(".key7Core").css("background-color", "black"); $(this).css("background-color", "White"); $(this).css("color", "white"); } else { $(this).css("background-color", "White"); } },
            mouseleave: function () { $(".doublekey7Core").css("background-color", "#DEDEDE"); $(".doublekey7Core").css("color", "Black"); $(".key7Core").css("background-color", "#DEDEDE"); }
        });

        //special purpose keys ---------------------------
        $("#shiftKey7Core").live("click", function () {
            if (host.upper == 0) {
                host.upper = 1;
                var imgURL = chrome.extension.getURL("/images/Bullet-green.png");
                $("#fdesc17Core").text("Uppercase is on");
                $(".statusLED7Core").css("background-image", "url('" + imgURL + "')");
            }
            else {
                host.upper = 0;
                var imgURL = chrome.extension.getURL("/images/Bullet-red.png");
                $("#fdesc17Core").text("Uppercase is off");
                $(".statusLED7Core").css("background-image", "url('" + imgURL + "')");
            }
        });
        $("#shiftKey7Core").live({
            mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "White"); },
            mouseleave: function () { $(this).css("background-color", "#DEDEDE"); }
        });

        $("#enterKey7Core").live("click", function () {
            el.readOnly = false;
            if ($(el).attr("id") == "7CIPtempID") {
                $(el).removeAttr("id");
                host.selected = "none";
            }
            el.focus();
            $("#7CoreInputProtection").remove();
            $("#auxBody7Core").remove();
            host.enabled = 0; next = -1;
        });
        $("#enterKey7Core").live({
            mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "White"); },
            mouseleave: function () { $(this).css("background-color", "#DEDEDE"); }
        });

        $("#backspaceKey7Core").live("click", function () { el.focus(); var content = el.value.toString(); var len = content.length; el.value = content.slice(0, (len - 1)); });
        $("#backspaceKey7Core").live({
            mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "White"); },
            mouseleave: function () { $(this).css("background-color", "#DEDEDE"); }
        });


        $("#spaceKey7Core").live("click", function () { var content = el.value.toString(); el.value = content + ' '; });
        $("#spaceKey7Core").live({
            mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "White"); },
            mouseleave: function () { $(this).css("background-color", "#DEDEDE"); }
        });

        $("#closeBtn7Core").live({
            mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("color", "red"); },
            mouseleave: function () { $(this).css("color", "black"); },
            click: function () { el.readOnly = false; if ($(el).attr("id") == "7CIPtempID") { $(el).removeAttr("id"); host.selected = "none"; } $("#7CoreInputProtection").remove(); $("#auxBody7Core").remove(); host.enabled = 0; next = -1; }
        });

        $("#expand7Core").live({
            mouseenter: function () {
                if (up == 0) {
                    $(this).css("cursor", "pointer");
                    var imgExpand = chrome.extension.getURL("/images/button_expand_hover.png");
                    $(this).css("background-image", "url('" + imgExpand + "')");
                }
                else {
                    $(this).css("cursor", "pointer");
                    var imgExpand = chrome.extension.getURL("/images/button_collapse_hover.png");
                    $(this).css("background-image", "url('" + imgExpand + "')");
                }
            },
            mouseleave: function () {
                if (up == 0) {
                    $(this).css("cursor", "pointer");
                    var imgExpand = chrome.extension.getURL("/images/button_expand.png");
                    $(this).css("background-image", "url('" + imgExpand + "')");
                }
                else {
                    $(this).css("cursor", "pointer");
                    var imgExpand = chrome.extension.getURL("/images/button_collapse.png");
                    $(this).css("background-image", "url('" + imgExpand + "')");
                }
            },
            click: function () {
                if (up == 0) {
                    var imgExpand = chrome.extension.getURL("/images/button_collapse.png");
                    $(this).css("background-image", "url('" + imgExpand + "')");
                    $("#coreCentral7Core").animate({ height: "438px" }, 1500);
                    $("#langPanel7Core").show(); up = 1;
                }
                else {
                    var imgExpand = chrome.extension.getURL("/images/button_expand.png");
                    $(this).css("background-image", "url('" + imgExpand + "')");
                    $("#langPanel7Core").hide();
                    $("#coreCentral7Core").animate({ height: "275px" }, 1500); up = 0;
                }
            }

        });

    };

    //instead of checking for the number of arguments in the function
    //prototypes we have created a function for each type of event triggering
    //(i.e. in frame click and right-click and normal click and right-click;

    function gcd(a, b) {
        while (a != b) {
            if (a > b) a = a - b;
            else b = b - a;
        }
        return a;
    }

    function generateRandom() {
        var random1 = Math.floor(Math.random() * 100);
        while (gcd(random1, 26) != 1) {
            random1 = Math.floor(Math.random() * 100);
        }
        host.random = random1;

    }

    function forceStopRandom() {
        if (host.random == -1) {
            host.random == 1;
        }
    }
   
 

    this.triggerClick = function () {
        var toBeSelected;

        if (arguments[0]) {
            if (arguments[0] == "rightClick") {
                chrome.extension.sendMessage({ "Type": "input", "Input": "TextField", "Page": (location.hostname) }, function (response) {

                    createKeyboard(response["Blackout"],response["SecurityLevel"]);


                });

            }
            else {
                //create global variable scope for the subfunction of the request
                //send request tot the extension in order to check if the input item clicked is a valid (enabled for value scrambling )
                chrome.extension.sendMessage({ "Type": "input", "Input": arguments[1], "Page": (location.hostname) }, function (response) {
                    toBeSelected = response["Status"];
                    if (toBeSelected == "true") {
                        createKeyboard(response["Blackout"], response["SecurityLevel"]);
                    }

                });
            }
        }

    }

    function createKeyboard(blackout, seclevel) {
        if (host.enabled == 0) {

            (blackout=="true") ? host.blackout = true : host.blackout = false;
            el = host.selected;
            el.readOnly = true;
            
           
            setTimeout(generateRandom, 3000);
            if (host.random == -1) {
                host.random = 1;
            }
          
            switch (seclevel) {
                case "1":
                    {
                        var allset = 0;
                        var current = 0;
                        while (allset < 28) {
                           
                            qwertyKeyboard[allset] = { character: String.fromCharCode((current % 26) + char), set: false };
                            current = (current + 1) % 26;
                            allset++;
                        }
                        break;
                    }
                case "2":
                    {
                        var allset = 0;
                        var current = (host.random % 26);
                       
                        for (var i = 0; i < 28; i++) {

                            qwertyKeyboard[i] = { character: String.fromCharCode((i + current) % 26 + char), set: false };
                        }
                       
                        break;

                    }
                case "3":
                    {
                        var allset = 0;
                        var current = 0;
                        while (allset < 28) {
                            qwertyKeyboard[allset] = { character: String.fromCharCode(((current + host.random) % 26) + char), set: false };
                            current = (current + host.random) % 26;
                            allset++;
                        }
                        break;
                    }
            
            }
         
            host.upper = 0; //set uppercase to false before opening editing box
            generateKeyboard(); //create the keyboard html code

            host.enabled = 1;

        }
    
    }

 

    function getInFrameElement(path) {
        var frames = [];
        var tmpDoc = document;
        for (var i = 0; i < path.length; i++) {
            var tmpframes1 = tmpDoc.getElementsByTagName("iframe");
            var tmpframes2 = tmpDoc.getElementsByTagName("frame");
            for (var index = 0; index < tmpframes1.length; index++) {
                frames.push(tmpframes1[index]);
            }
            for (var index = 0; index < tmpframes2.length; index++) {
                frames.push(tmpframes2[index]);
            }
            tmpDoc = frames[path[i]].contentDocument;
            frames = [];
        }

        return tmpDoc.getElementById(host.selected);

    }

    //pre: the webpage contains a body (i.e the <body></body> tags)
    //the function creates the html for the keyboard as well as sticking it to the body of the web page
    //uses $("body") to find the body of the webpage
    function generateKeyboard() {

        var panel = '<div id = "keyBoard7Core"><div id="keyRow0" class="Row7Core">' +
                        '<div class="doublekey7Core" ><div class="special7Core">~</div><div class="basic7Core">`</div></div>' +
                        '<div class="doublekey7Core" ><div class="special7Core">!</div><div class="basic7Core">1</div></div><div class="doublekey7Core" ><div class="special7Core">@</div><div class="basic7Core">2</div></div>' +
                        '<div class="doublekey7Core" ><div class="special7Core">#</div><div class="basic7Core">3</div></div><div class="doublekey7Core" ><div class="special7Core">$</div><div class="basic7Core">4</div></div>' +
                        '<div class="doublekey7Core" ><div class="special7Core">%</div><div class="basic7Core">5</div></div><div class="doublekey7Core" ><div class="special7Core">^</div><div class="basic7Core">6</div></div>' +
                        '<div class="doublekey7Core" ><div class="special7Core">&</div><div class="basic7Core">7</div></div><div class="doublekey7Core" ><div class="special7Core">*</div><div class="basic7Core">8</div></div>' +
                        '<div class="doublekey7Core" ><div class="special7Core">(</div><div class="basic7Core">9</div></div><div class="doublekey7Core" ><div class="special7Core">)</div><div class="basic7Core">0</div></div>' +
                        '<div class="doublekey7Core" ><div class="special7Core">_</div><div class="basic7Core">-</div></div><div class="doublekey7Core" ><div class="special7Core">+</div><div class="basic7Core">=</div></div>' +
                        '<div id="backspaceKey7Core" >Backspace</div>' +
                        '</div>' +
                        '<div id="keyRow1" class="Row7Core">' +
                        '<div class="alignmentkey7Core" style="float:left; position:relative; width:50px; height: 40px;"></div>' +
                        '<div class="key7Core" >' + getKey() + '</div><div class="key7Core" >' + getKey() + '</div>' +
                        '<div class="key7Core" >' + getKey() + '</div><div class="key7Core" >' + getKey() + '</div>' +
                        '<div class="key7Core" >' + getKey() + '</div><div class="key7Core" >' + getKey() + '</div>' +
                        '<div class="key7Core" >' + getKey() + '</div><div class="key7Core" >' + getKey() + '</div>' +
                        '<div class="key7Core" >' + getKey() + '</div><div class="key7Core" >' + getKey() + '</div>' +
                        '<div class="doublekey7Core" ><div class="special7Core">{</div><div class="basic7Core">[</div></div><div class="doublekey7Core" ><div class="special7Core">}</div><div class="basic7Core">]</div></div>' +
                        '<div class="doublekey7Core" ><div class="special7Core">|</div><div class="basic7Core">\\</div></div>' +
                        '</div>' +
                        ' <div id="keyRow2" class="Row7Core">' +
                        '<div class="alignmentkey7Core" style="float:left; position:relative; width:50px; height: 40px;"></div>' +
                        ' <div class="key7Core" >' + getKey() + '</div>' +
                        ' <div class="key7Core" >' + getKey() + '</div>' +
                        ' <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        ' <div class="key7Core" >' + getKey() + '</div>' +
                        ' <div class="key7Core" >' + getKey() + '</div>' +
                        ' <div class="key7Core" >' + getKey() + '</div>' +
                        ' <div class="doublekey7Core" ><div class="special7Core">:</div><div class="basic7Core">;</div></div>' +
                        ' <div class="doublekey7Core" ><div class="special7Core">\"</div><div class="basic7Core">\'</div></div>' +
                        '  <div id="enterKey7Core" >Enter</div>' +
                        '  </div>' +
                        '  <div id="keyRow3" class="Row7Core">' +
                        '<div class="alignmentkey7Core" style="float:left; position:relative; width:50px; height: 40px;"></div>' +
                        '  <div id="shiftKey7Core">Uppercase <br /> Shift</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div> ' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="key7Core" >' + getKey() + '</div>' +
                        '  <div class="doublekey7Core" ><div class="special7Core"><</div><div class="basic7Core">,</div></div>' +
                        '  <div class="doublekey7Core" ><div class="special7Core">></div><div class="basic7Core">.</div></div>' +
                        ' <div class="doublekey7Core" ><div class="special7Core">?</div><div class="basic7Core">/</div></div>' +
                        '   </div>                  ' +
                        '  <div id="keyRow4" class="Row7Core">' +
                        '<div class="alignmentkey7Core" style="float:left; position:relative; width:200px; height: 40px;"></div>' +
                        '  <div id="spaceKey7Core" >Space</div>' +
                        '   </div>                  ' +
                        ' </div>';





        //add the keyboard header containing the background image which is 550px wide with border inside which may require minor "div" re-alignments
        //(i.e margin left increased by a few pixels and margin right decreased by a few pixels in order to adjust
        //the layout
        var header = '<div id="logo7CoreInputProtection"><div id="headerContent7Core" style="position:relative; margin-left: 24px; margin-right:16px;"><div style="height:65px;"><div id="posTool7Core" style="float:left; position:relative; width:160px; height:65px;">' +
        '</div><div id="SeptaimgLogo" style="float:left; position:relative; height:65px; width:360px; background-repeat:no-repeat; "></div>' +
        '<div style="height:65px; width:30px; float:left; position:relative;"><div id="closeBtn7Core" style="height:15px; font-size: 14px;"><b>X</b></div></div></div>' +
        '<div class="function7Core"><div class="statusLED7Core"></div><div class="description7Core" id="fdesc17Core">Uppercase is off</div></div></div></div>';

        var imgKeyBoardUp = chrome.extension.getURL("/images/shadow-up.png");
        var imgKeyBoardContent = chrome.extension.getURL("/images/shadow-content.png");
        var imgKeyBoardBottom = chrome.extension.getURL("/images/shadow-bottom.png");

        //$("html").append("<body></body>");
        //alert(document.body);
        if (document.body.tagName.toLowerCase() == "frameset") {
            $("html").append('<div style="width:100%; height:100%; position: fixed; top: 0px; left: 0px;" id="auxBody7Core"></div>');

            $("#auxBody7Core").append('<div id="7CoreInputProtection" class="Protection7Core" ></div>');
          
        }
        else {
            $("body").append('<div id="7CoreInputProtection" class="Protection7Core" ></div>');
        }

        $("#7CoreInputProtection").append(header);
        //setting the background of the header
        $("#logo7CoreInputProtection").css("background-image", "url('" + imgKeyBoardUp + "')");
        //append the central portion of the keyboard (the mid div)which contains the key buttons and the 
        //extra language panel. This central portion will get resized whenever the user click the expand button
        $("#7CoreInputProtection").append('<div id="coreCentral7Core" style="width:100%; background-repeat: repeat-y; position:relative; min-height:275px;"></div>');
        //set the background image of the central panel
        $("#coreCentral7Core").css("background-image", "url('" + imgKeyBoardContent + "')");

        //append the content to the central panel in order to be able to align it properly (i.e margin-left 23 px and margin-right: 17px;)
        $("#coreCentral7Core").append('<div id="mainContent7Core" style="width: 546px; z-index:3; position:relative; margin-left:23px; margin-right:17px;"></div>');

        //append the keyboard to the mainContent area
        $("#mainContent7Core").append(panel);

        //append the bottom part of the rendered keyboard for nice visual effects
        $("#7CoreInputProtection").append('<div id="bottomContent7Core" style="height:36px; width:100%; z-index:1; position:absolute; bottom:-36px; left: 0px;"></div>');
        $("#bottomContent7Core").css("background-image", "url('" + imgKeyBoardBottom + "')");

        //generate the auxiliary language panel that displays
        //the user created languages
        getSelectedLanguagePanel();
        var imgURL = chrome.extension.getURL("/images/Bullet-red.png");
        var imgLogoURL = chrome.extension.getURL("/images/fangos.png");

        //add dynamic content  to the statusLED
        $(".statusLED7Core").css("background-image", "url('" + imgURL + "')");
        //show the keyboard and make it draggable
       
        if (document.body.tagName.toLowerCase() == "frameset") {
            // $("html").append('<div style="width:100%; height:100%; position: fixed; top: 0px; left: 0px;" id="auxBody7Core"></div>');
            $("#posTool7Core").append('<div style="position: relative; float:left; width: 50px; height: 50px; margin-left: 10px; margin-top:10px; border-radius: 5px;">' +
          '<div id="fitUpLeft7Core" style="width: 25px; float: left; position: relative; height:25px; background-color: Red;"></div>' +
          '<div id="fitUpRight7Core" style="width: 25px; float: left; position: relative; height:25px; background-color: Red;"></div>' +
          '<div id="fitBottomLeft7Core" style="width: 25px; float: left; position: relative; height:25px; background-color: Red;"></div>' +
          '<div id="fitBottomRight7Core" style="width: 25px; float: left; position: relative; height:25px; background-color: Red;"></div>' +
          '</div> ');
            $("#fitUpLeft7Core").live({
                click: function () { $("#7CoreInputProtection").css({ "top": "10px", "left": "10px", "bottom": "auto", "right": "auto" }); },
                mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "Black"); },
                mouseleave: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "Red"); }
            });
            $("#fitUpRight7Core").live({
                click: function () { $("#7CoreInputProtection").css({ "top": "10px", "left": "auto", "bottom": "auto", "right": "10px" }); },
                mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "Black"); },
                mouseleave: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "Red"); }
            });
            $("#fitBottomLeft7Core").live({
                click: function () { $("#7CoreInputProtection").css({ "top": "auto", "left": "10px", "bottom": "10px", "right": "auto" }); },
                mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "Black"); },
                mouseleave: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "Red"); }
            });
            $("#fitBottomRight7Core").live({
                click: function () { $("#7CoreInputProtection").css({ "top": "auto", "left": "auto", "bottom": "10px", "right": "10px" }); },
                mouseenter: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "Black"); },
                mouseleave: function () { $(this).css("cursor", "pointer"); $(this).css("background-color", "Red"); }
            });
        }
        else {
            $("#7CoreInputProtection").draggable({ handle: "div#logo7CoreInputProtection" });
        }
        $("#7CoreInputProtection").show();



    }

    //go to the next key in the qwertyKeyboard generated set
    function getKey() {
        next++;
        //alert(qwertyKeyboard[next].character);
        return (qwertyKeyboard[next].character);
    }

    //the function will display (if enabled) the auxiliary language panel
    //containing the current language as set by the user
    //keys are displayed 15 in a row and on as many rows as necessary
    //in a div with the overflow-y set to scroll
    function getSelectedLanguagePanel() {
        var stringText = "";
        chrome.extension.sendMessage({ "Type": "languageInfo", "Language": "getCurrent" }, function (response) {
            //read all the keys here, put 7/8 in a row on multiple rows
            //keys format Key ({main, secondary});
            if (response["isSet"]) {
                var languagePanel = '<div style="position:relative; height:19px; background-color:White; border-radius: 3px; z-index: 3;"><div id="expand7Core" style="margin-left:auto; margin-right:auto; height:19px; width: 19px;"></div>' +
                                '</div><div id="langPanel7Core" style="height: 150px; max-height:150px; overflow-y: scroll; margin-top: 5px;"></div>';
                $("#mainContent7Core").append(languagePanel);
                for (var i = 0; i < response["Values"].length; i++) {
                    if (i % 15 == 0) {
                        stringText = stringText + '<div class="Row7Core" >';

                    }
                    stringText = stringText + '<div class="doublekey7Core"><div class="special7Core">' + response["Values"][i].special + '</div><div class ="basic7Core">' + response["Values"][i].basic + '</div></div>';

                    if (i % 15 == 14) {
                        stringText = stringText + '</div>';
                    }

                }
                //stringText = stringText;
                $("#langPanel7Core").append(stringText);
                var imgExpand = chrome.extension.getURL("/images/button_expand.png");
                $("#expand7Core").css("background-image", "url('" + imgExpand + "')");
                $("#langPanel7Core").hide();
            }

        });

    }
}
