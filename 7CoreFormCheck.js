﻿/*
* Author: Andrei Bara
* Company: septacore.com
* Product Name: 7 Core Secure Input
* URL: http://www.septacore.com
* Version: 1.0
* Copyright © 2011 Andrei Bara and "septacore.com"
* Licence agreement: The source code that comes with this application IS NOT open source.
*                    It is therefore illegal to redistribute, extract, modify
*                    or alter the contents of the program
*                    without the express notification of the copyright holder/s
*                    and of their approval.
*                    
*/

function formCheck(form) {
    var dictionaryCard = ["creditcard", "credit card", "card credit", "card"];
    var dictionaryCardNumber = ["number", "no", "no.", "nr.", "nr"];
    var dictionaryAddress = ["address", "adres", "addres","adress"];
    var dictionaryAddressPostal = ["postal code", "zip code", "zip", "postal", "post code","post  code", "post   code", "post    code", "postcode", "postalcode"];
    var dictionaryCVV = ["cvc", "cvv", "cvv2", "cvvc", "csc", "cvc2", "cvd", "ccv" ];
    var dictionaryExpiration = ["expiration date", "expiration", "date"];
    var totalPercentage = 0;

    var cardFound = false;

    //*------------PayPal protection---------
    var totalPercentagePayPal = 0;
    var dictionaryPayPal = ["pay pal","paypal"];
    var dictionaryLogIn = ["log in", "login", "sign in","sign up"];
    var dictionaryPassword = ["password"];
    var dictionaryEmail = ["email"];
    var passwordBoxCount = 0;
    //--------------------------------------*/


    var inputs = form.getElementsByTagName("input");
    var submitExists = false;
    var textBoxCount = 0;
    var textToSearchIn = $(form).text();

    for (var i = 0; i < inputs.length; i++) {
        if (inputs[i].getAttribute("type") == "text") {
            textBoxCount++;
        }
        else if(inputs[i].getAttribute("type") == "password"){
            passwordBoxCount++;
        }
        else if (inputs[i].getAttribute("type") == "submit") {
            submitExists = true;
        }
    }

    for (var index = 0; index < dictionaryCard.length; index++) {
        var patt = new RegExp(dictionaryCard[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            totalPercentage += 30;
            cardFound = true;
            break;

        }
    }
    for (var index = 0; index < dictionaryCardNumber.length; index++) {
        var patt = new RegExp(dictionaryCardNumber[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            if (cardFound)
                totalPercentage += 20;
            else {
                totalPercentage += 10;
            }
            break;
        }
    }
    for (var index = 0; index < dictionaryAddress.length; index++) {
        var patt = new RegExp(dictionaryAddress[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            if (cardFound) {
                totalPercentage += 10;
            }
            else {
                totalPercentage += 5;
            }
            break;
        }
    }
    for (var index = 0; index < dictionaryCVV.length; index++) {
        var patt = new RegExp(dictionaryCVV[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            if (cardFound) {
                totalPercentage += 30;
            }
            else {
                totalPercentage += 10;
            }
            break;
        }
    }
    for (var index = 0; index < dictionaryExpiration.length; index++) {
        var patt = new RegExp(dictionaryExpiration[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            if (dictionaryExpiration[index] != "date")
                totalPercentage += 10;
            else {
                totalPercentage += 5;
            }
            break;
        }
    }

    //---------------PayPal checking code-------------
    for (var index = 0; index < dictionaryPayPal.length; index++) {
        var patt = new RegExp(dictionaryPayPal[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            totalPercentagePayPal += 35;
            break;
        }
    }
    
    for (var index = 0; index < dictionaryPassword.length; index++) {
        var patt = new RegExp(dictionaryPassword[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            totalPercentagePayPal += 20;
            break;
        }
    }
     for (var index = 0; index < dictionaryEmail.length; index++) {
        var patt = new RegExp(dictionaryEmail[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            totalPercentagePayPal += 20;
            break;
        }
    }
     for (var index = 0; index < dictionaryLogIn.length; index++) {
        var patt = new RegExp(dictionaryLogIn[index], "i");
        if (textToSearchIn.search(patt) >= 0) {
            totalPercentagePayPal += 25;
            break;
        }
    }
    //-----------------end of PayPal check Code------------------

    //----------------result output------------------------------
    return ((totalPercentage >= 60 && submitExists && textBoxCount>2) || (totalPercentagePayPal >= 65 && passwordBoxCount>=1 && submitExists));


}

function displayWarning() {
    
    var imgLogoURL = chrome.extension.getURL("/images/7coreSecureInput.png");

    $("#warningLogo7Core").css("background-image", "url('" + imgLogoURL + "')");
    var htmlString = '<div id="warningDialog7Core" class="test"><div id="warningLogo7Core" style="width: 15%; position:relative; float:left; height:40px;"></div><div style="position:relative; float:left; width:70%; height:40px;">' +
                        'warning this is a bad</div><div style="position:relative; float:left; width: 15%; height:40px;"><div id="closeWarning7Core" style="height:20px; position:relative; float:right;"><b>X</b></div></div></div>';
    $("body").append(htmlString);
}