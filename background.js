     /*
        * Author: Andrei Bara
        * Company: septacore.com
        * Product Name: 7 Core Secure Input
        * URL: http://www.septacore.com
        * Version: 1.0
        * Copyright © 2011 Andrei Bara and "septacore.com"
        * Licence agreement: The source code that comes with this application IS NOT open source.
        *                    It is therefore illegal to redistribute, extract, modify
        *                    or alter the contents of the program
        *                    without the express notification of the copyright holder/s
        *                    and of their approval.
        *                    
        */

        if (localStorage["Pages"] == undefined) {
            localStorage.setItem("Pages", "AllPages");

        }

        if (localStorage["TextField"] == undefined) {
            localStorage.setItem("TextField", "false");

        }
        if (localStorage["TextArea"] == undefined) {
            localStorage.setItem("TextArea", "false");
        }
        if (localStorage["PasswordField"] == undefined) {
            localStorage.setItem("PasswordField", "false");
        }
        if (localStorage["CustomPages"] == undefined) {
            localStorage.setItem("CustomPages", JSON.stringify([]));
        }


        if (localStorage["ShowLanguage"] == undefined) {
            localStorage.setItem("ShowLanguage", "false");
        }

        if (localStorage["UserDefinedLanguages"] == undefined) {
            localStorage.setItem("UserDefinedLanguages", JSON.stringify([]));
        }

        if (localStorage["EnableSecurity"] == undefined) {
            localStorage.setItem("EnableSecurity", "true");
        }

        if (localStorage["EnableFrames"] == undefined) {
            localStorage.setItem("EnableFrames", "true");
        }

        if (localStorage["Blackout"] == undefined) {
            localStorage.setItem("Blackout", "false");

        }
        if (localStorage["SecurityLevel"] == undefined) {
            localStorage.setItem("SecurityLevel", "2");
        }


        if (localStorage["WebCheckEnabled"] == undefined) {
            localStorage.setItem("WebCheckEnabled", "true");
        }

            localStorage.setItem("WebCheckDomains", '{ "com" : ["fidelity.com","netflix.com","hulu.com","mail.google.com","google.com","amazon.com", "paypal.com", "www.paypal.com", "history.paypal.com", "sandbox.paypal.com", "www.rbsdigital.com", "rbsdigital.com", "rbs.com", "www.rbs.com", "db.com",' +
            '"www.db.com", "online.citbank.com", "www.bankofamerica.com", "www.ebay.com","ebay.com","dzbank.com","www.dzbank.com","abnamro.com","www.abnamro.com","santander.com","www.santander.com","natwest.com","www.natwest.com","bnpparibas.com",'+
            '"www.bnpparibas.com","lcl.com","www.lcl.com","ing.com","www.ing.com","lloydstsb.com","www.lloydstsb.com","www.hsbc.com","hsbc.com","ml.com","www.ml.com","www.anb.com","anb.com","www.jpmorgan.com","jpmorgan.com","www.asbhawaii.com","asbhawaii.com",'+
            '"www.bnymellon.com","bnymellon.com","www.capitalone.com","capitalone.com","www.bankunited.com","bankunited.com","www.rbcroyalbank.com","rbcroyalbank.com","www.citigroup.com","citigroup.com","www.gs.com","gs.com","www.credit-suisse.com","credit-suisse.com",' +
            '"www.ubs.com","ubs.com","raiffeisen.com","www.raiffeisen.com","gogecapital.com","www.gogecapital.com","jpmorganchase.com","www.jpmorganchase.com","wellsfargo.com","facebook.com"],' +
            '"co.uk": ["rbs.co.uk","www.rbs.co.uk","mail.google.co.uk","google.co.uk","amazon.co.uk","co-operativebank.co.uk","santander.co.uk","www.santander.co.uk","barclays.co.uk","www.barclays.co.uk","natweststockbrokers.co.uk","www.natweststockbrokers.co.uk","ingdirect.co.uk","www.ingdirect.co.uk","hsbc.co.uk","www.hsbc.co.uk",' +
            '"www.bankofscotland.co.uk","bankofscotland.co.uk","www.bmsavings.co.uk","bmsavings.co.uk","www.cheltglos.co.uk","cheltglos.co.uk","www.church-house.co.uk","church-house.co.uk","www.halifax.co.uk","halifax.co.uk"],' +
            '"de" : ["deutsche-bank.de","www.deutsche-bank.de","rbs.de","google.de","www.rbs.de","www.lbb.de","lbb.de","www.berlinhyp.de","berlinhyp.de","www.berliner-bank.de","berliner-bank.de","www.berliner-sparkasse.de","berliner-sparkasse.de","commerzbank.de","www.commerzbank.de",'+
            '"comdirect.de","www.comdirect.de","www.postbank.de","postbank.de","www.dslbank.de","dslbank.de","norisbank.de","www.norisbank.de","bayernlb.de","www.bayernlb.de","hsh-nordbank.de","www.hsh-nordbank.de","ebay.de"],'+
            '"es":["bancosantander.es","www.bancosantander.es"],'+
            '"se":["handelsbanken.se","www.handelsbanken.se"],'+
            '"fr":["societegenerale.fr","www.societegenerale.fr","google.fr"],'+
            '"ie":["www.aib.ie"],' +
			'"org":["fairwinds.org"],'+
            '"au":["ebay.au"],'+
            '"ch":["www.aekbank.ch","aekbank.ch","akbprivatbank.ch","www.akbprivatbank.ch","axabank.ch","www.axabank.ch","www.bnpparibas.ch","bnpparibas.ch","arvest.ch","www.arvest.ch","bps-suisse.ch","www.bps-suisse.ch","eek.ch","www.eek.ch"]}');


            chrome.cookies.get({ "url": "http://septacore.com", "name": "CakeCookie[fangos]" }, function (cookie) {
                if (cookie == undefined || cookie.value != "activated") {              
                    localStorage.setItem("Pages", "AllPages");
                    localStorage.setItem("TextField", "false");
                    localStorage.setItem("TextArea", "false");
                    localStorage.setItem("PasswordField", "false");
                    localStorage.setItem("CustomPages", JSON.stringify([]));
                    localStorage.setItem("ShowLanguage", "false");
                    localStorage.setItem("UserDefinedLanguages", JSON.stringify([]));
                    localStorage.setItem("EnableSecurity", "false");
                    localStorage.setItem("EnableFrames", "false");
                    localStorage.setItem("Blackout", "false");
                    localStorage.setItem("SecurityLevel", "2");
                    localStorage.setItem("WebCheckEnabled", "false");
                }
            });


        $(document).ready(function () {
            //alert(JSON.stringify([{ "Extension": ".com", "Domains": ["google.com", "paypal.com"] }, { "Extension": ".co.uk", "Domains": ["rbs.co.uk"]}]));

			/*
            try {
                var req = new XMLHttpRequest();
                req.open("GET", "http://www.septacore.com/products/extensions/extensions.php?appname=7CoreSecureInput", true);
                req.onload = showUpdate;
                req.send(null);

                function showUpdate() {

                    var data = JSON.parse(req.responseText);


                    if (data != undefined) {
                        var serverVersion = data["Version"];
                        var serverUpdateText = data["Message"];
                        var serverUpdateLink = data["Link"];
                        var serverAnnouncement = data["Announcement"];
                        localStorage["latestVersion"] = serverVersion;
                        chrome.management.get("pedijpnaiabopojfjbjbmmfoggenjegg", function extData(info) {

                            if (serverAnnouncement == true) {
                                localStorage["needsUpdate"] = true;
                                localStorage["updateText"] = serverUpdateText;
                                localStorage["updateLink"] = serverUpdateLink;
                            }
                            else {
                                localStorage["needsUpdate"] = false;
                            }
                        });
                    }

                }
            }
            catch (err) {
            }*/

        });

        //to add the case when the local storage has not been defined
        //define pages
        //define language show
        //define language list

        function doWebCheck() {
          //  alert("tabChanged");
		 
            if (localStorage["WebCheckEnabled"] == "true") {
                chrome.tabs.query({'active': true, 'windowId': chrome.windows.WINDOW_ID_CURRENT}, function (tabs) {
                    var domain = (tabs[0].url).match(/(http|https):\/\/([^\/]+)/g)[0].match(/[^\/]+/g)[1];
                    var data = JSON.parse(localStorage["WebCheckDomains"]);
                    //alert(domain);
                    var domainExtension;
                    var domainsParse = domain.match(/(co.uk|com|au|ro|fr|info)/g);
                    if (domainsParse)
                        domainExtension = domainsParse[0];
                    var found = false;

                    if (data[domainExtension]) {
                        for (var i = 0; i < data[domainExtension].length; i++) {

                            // alert(domain.indexOf("." + data[domainExtension][i]));
                            if (data[domainExtension][i] == domain || domain.indexOf("." + data[domainExtension][i]) >= 0) {

                                chrome.browserAction.setIcon({ path: "icon-medium-green.png" });
                                // alert("try");
                                found = true;
                                break;
                            }

                        }
                    }
                    if (!found) {
                        //alert("tryFail");
						
                        chrome.browserAction.setIcon({ path: "icon-medium.png" });

                    }
                   
                    
                });
            }
        }
        chrome.tabs.onSelectionChanged.addListener(function (tabId, selectInfo) {
            doWebCheck();
        });

        chrome.extension.onMessage.addListener(function (request, sender, sendResponse) {


            if (request["Type"] == "input") {
                if (localStorage["Pages"] == "AllPages") {

                    sendResponse({ "Status": localStorage[request["Input"]], "Blackout": localStorage["Blackout"], "SecurityLevel": localStorage["SecurityLevel"] });
                }
                else if (localStorage["Pages"] == "CustomPages") {
                    var pagesTmpJSON = localStorage["CustomPages"];
                    var pagesTmp = JSON.parse(pagesTmpJSON);
                    var index = findElem(pagesTmp, request["Page"]);
                    if (index >= 0) {

                        sendResponse({ "Status": pagesTmp[index][request["Input"]].toString(), "Blackout": localStorage["Blackout"], "SecurityLevel": localStorage["SecurityLevel"] });
                    }
                    else {
                        sendResponse({ "Status": "false", "Blackout": localStorage["Blackout"], "SecurityLevel": localStorage["SecurityLevel"] });
                    }
                }
                else {
                    sendResponse({});
                }
            }
            else if (request["Type"] == "languageInfo") {

                if (request["Language"] == "getCurrent") {
                    var currentLang = localStorage["CurrentLanguage"];
                    if (currentLang != undefined && localStorage["ShowLanguage"] == "true") {
                        sendResponse({ "isSet": true, "Values": JSON.parse(localStorage[currentLang]) });
                    }
                    else {
                        sendResponse({ "isSet": false, "Values": [] });
                    }

                }
                else {
                    sendResponse({});
                }

            }
            else if (request["Type"] == "tab") {
                chrome.tabs.create({ "url": request["url"] });
            }
            else if (request["Type"] == "Extras") {
                sendResponse({ "FramesEnabled": (localStorage["EnableFrames"] == "true") ? true : false, "SecurityEnabled": (localStorage["EnableSecurity"] == "true") ? true : false });

            }
            else if (request["Type"] == "DomainCheck") {
                doWebCheck();
                sendResponse({});
            }
            else {
                sendResponse({});
            }


        });

        function findElem(arrayPages, pagename) {
            for (var i = 0; i < arrayPages.length; i++) {
                if (arrayPages[i].PageUrl == pagename) {
                    return i;
                }
            }
            return -1;
        }



        // A generic onclick callback function.
        function genericOnClick(info, tab) {
            //send request to the content script in the current tab

            var fURL = info.frameUrl;
            if (fURL == undefined) {
                fURL = "none";
            }

            switch (info.menuItemId) {
                case "trans":
                    {
                        chrome.tabs.sendMessage(tab.id, { "Type": "event", "Action": "invert", "NewType": "password" }, function (response) {
                            return true;

                        }); break;
                    }
                case "rev":
                    {
                        chrome.tabs.sendMessage(tab.id, { "Type": "event", "Action": "invert", "NewType": "textbox" }, function (response) {
                            return true;

                        }); break;
                    }
                case "prot":
                    {
						
                        chrome.tabs.sendMessage(tab.id, { "Type": "event", "Action": "encrypt", "FrameURL": fURL, "FramesEnabled": (localStorage["EnableFrames"] == "true") ? true : false }, function (response) {
                            return true;

                        }); break;
                    }
				default:
					console.log("Unknown option has been selected. If this is a bug, please contact support@septacore.com");

            }






        }

        // Create one test item for each context type.
        var contexts = ["page", "selection", "link", "editable", "image", "video",
                "audio"];
        chrome.contextMenus.create({ "title": "Protect this", "contexts": ["editable"], "id": "prot", "onclick": genericOnClick });
        chrome.contextMenus.create({ "title": "Password transform", "contexts": ["editable"],"id": "trans", "onclick": genericOnClick });
        chrome.contextMenus.create({ "title": "Revert to textbox", "contexts": ["editable"], "id": "rev", "onclick": genericOnClick });
                
           
             
