﻿/*
        * Author: Andrei Bara
        * Company: septacore.com
        * Product Name: 7 Core Secure Input
        * URL: http://www.septacore.com
        * Version: 1.0
        * Copyright © 2011 Andrei Bara and "septacore.com"
        * Licence agreement: The source code that comes with this application IS NOT open source.
        *                    It is therefore illegal to redistribute, extract, modify
        *                    or alter the contents of the program
        *                    without the express notification of the copyright holder/s
        *                    and with their approval.
        *                    
        */

// random generation of the keyboard letters for mouse pointing obfuscation


var tempBoard7Core = new Keyboard7Core();
var globalIframePath7Core;
var iframesEnabled = false;
var securityCheckEnabled7Core = false;



/*
-------------check if the forms look like credit card processing forms-----------
*/

chrome.extension.sendMessage({ "Type": "DomainCheck" }, function (response) { });
$(document).ready(function () {
chrome.extension.sendMessage({"Type" : "Extras"}, function (response){
    iframesEnabled7Core= response["FramesEnabled"];
  
    securityCheckEnabled7Core = response["SecurityEnabled"];
    if(response["SecurityEnabled"]){
        securityCheck7Core(document.getElementsByTagName("form"));
    }
    if(response["FramesEnabled"]){
        recursiveEnableIframe7Core(document);
    }
    /*if(response["WebCheckEnabled"]){
        crossCheckDomain7Core(document.domain,response["DomainList"]);
    }*/
});
    

});

function crossCheckDomain7Core(dmn,dmnList){
   
}

//the function will check each form in the page to see
//if any sensitive data is required from the user
function securityCheck7Core(forms) {
    if (forms != undefined) {
        for (var i = 0; i < forms.length; i++) {

            if (formCheck(forms[i])) {
          
                alert("7 Core Secure Input has detected that: \n" +
            "* This website might require sensitive data to be stored or typed in. \n" +
            "* Please make sure that you are on the right website " +
            "  and only then type in your data. \n" +
            "* We recommend you use the 7 Core Secure Input Keyboard");
                break;
            }
        }
    }
}



chrome.extension.onMessage.addListener(
		function (request, sender, sendResponse) {
			//alert("Message received!");
		    if (request["Type"] == "event") {
		        if (request["Action"] == "encrypt") {
                    var sameDomain = true;
                    if(request["FramesEnabled"]){
		                    if (request["FrameURL"] != "none") {
		                 
                                
		                        var frameDomain = request["FrameURL"].match(/(http|https):\/\/([^\/]+)/g)[0];
		                        var cDoc = document.domain.match(/([^\.]+)/g);
		                        var docDomain = document.URL.match(/(http|https):\/\/([^\/]+)/g)[0];
		                        if (docDomain != frameDomain) {
                                    sameDomain = false;
                                 
		                            alert("7 Core Secure Input : \n" +
                                    "* The frame you are trying to access is on a different domain. \n" +
                                    "* For security reasons you cannot edit this field in here, but a new window \n" +
                                    "  containing the frame will open.");
		                            chrome.extension.sendMessage({ "Type": "tab", "url": request["FrameURL"] }, function (response) { });
		                        }
		                    }
                        }

                    if(tempBoard7Core.enabled == 0){
                        if(request["FrameURL"] != "none"){  
                            if(request["FramesEnabled"] && sameDomain){
                                 $(tempBoard7Core.selected).val(null);          
                                 tempBoard7Core.triggerClick("rightClick");    
		                    }
                            else{
                                alert("7 Core Secure Input : Frames support not enabled or not working properly!" + sameDomain);
                            }

                       }  
                       else{
                            $(tempBoard7Core.selected).val(null);  
                           tempBoard7Core.triggerClick("rightClick");
                      }               
                    
                   }

		          
		            sendResponse({});
		        }
                else if(request["Action"] == "invert"){
                  
                    if($(tempBoard7Core.selected).get(0).tagName.toString().toLowerCase()=="input"){
                      switch(request["NewType"]){
                        case "password" :
                        {
                        $(tempBoard7Core.selected).prop("type","password");
                        break;
                        }
                        case "textbox" :
                        {
                         $(tempBoard7Core.selected).prop("type","text");
                         break;
                        }
                      }
                       
                    }
                }
		        else
		            sendResponse({});
		    }
		    else if (request["Type"] == "pageDetails") {
		        if (request["Page"] == "currentPage") {
		            currentPage = location.hostname;
		            var currentPageQuery = location.search;

		            sendResponse({ "Page": currentPage });
		        }
		        else
		            sendResponse({});
		    }
		    else {
		        sendResponse({});
		    }

		}

    );

    //recursively register events and check frames
    //untill the last frame
    
function recursiveEnableIframe7Core(outerDocument) {
   
    if (outerDocument != undefined) {
        var iframes = outerDocument.getElementsByTagName("iframe");
        var simpleFrames = outerDocument.getElementsByTagName("frame");
        var index =0;
        var frames= [];
        for(var i =0 ; i< iframes.length; i++){
            frames.push(iframes[i]);
           
        }
        for(var i = 0; i< simpleFrames.length; i++){
          frames.push(simpleFrames[i]);
         
        }
        for (var i = 0; i < frames.length; i++) {
           
            var d;
            var docDomain = "same";
            var frameDomain = "same", cDoc;
            if (frames[i].getAttribute("src") != undefined) {
            var de = frames[i].getAttribute("src").match(/(http|https):\/\/([^\/]+)/g);
                if (de != undefined) {
                   
                    frameDomain = de[0];
                    cDoc = document.domain.match(/([^\.]+)/g);
                    docDomain = document.URL.match(/(http|https):\/\/([^\/]+)/g)[0];
                }
                else{
                }
            }
            
            if (docDomain == frameDomain) {
               
                regEvents(frames[i]);
                if(frames[i].contentDocument != undefined){
                    securityCheck7Core(frames[i].contentDocument.getElementsByTagName("form"));
                }

                //stop if the frame that is going to be accessed point to the
                //parent webpage/document
                // check if contentDocument exists !!!!
                if(frames[i].contentDocument.URL == document.URL){
                    return;
                }
              

                frames[i].onload = function () {
                    if(this.contentDocument != undefined){
                         securityCheck7Core(this.contentDocument.getElementsByTagName("form"));
                     }
                    
                    regEvents(this);
                    recursiveEnableIframe7Core(this.contentDocument);
                  
                }

                recursiveEnableIframe7Core(frames[i].contentDocument);
            }
         

        }
    }
}
//register the click events

function regEvents(frame) {
  
    var frameDocument = frame.contentDocument;
    if (frame.contentDocument != undefined) {
        var frameBody = frame.contentDocument.body;


        if (frameBody != undefined) {
        
            $(frameDocument).mousedown(function (e){
                
                    if (e.which == 1) {
                        if ($(e.target).is("textarea") || $(e.target).prop("type") == "email"  || $(e.target).prop("type") == "text" || $(e.target).prop("type") == "password" || ($(e.target).prop("type")==undefined && $(e.target).prop("tagName")=="INPUT")){
                         
                            tempBoard7Core.selected = e.target;
                            if($(e.target).is("textarea")){
                            
                                tempBoard7Core.triggerClick("leftClick","TextArea");
                            }
                            else if($(e.target).attr("type") == "text"){
                                tempBoard7Core.triggerClick("leftClick","TextField");
                            }
                            else if($(e.target).attr("type") == "password"){
                                tempBoard7Core.triggerClick("leftClick", "PasswordField");
                            }
                            else if($(e.target).attr("type")==undefined && (e.target.tagName=="INPUT")){
                                 tempBoard7Core.triggerClick("leftClick","TextField");
                            }
                        }
                        
                    }
                    else if (e.which == 3) {
                        if ($(e.target).is("textarea") || $(e.target).prop("type") == "email"  || $(e.target).prop("type") == "text" || $(e.target).prop("type") == "password" || ($(e.target).prop("type")==undefined && $(e.target).prop("tagName")=="INPUT")){
                            
                            
                            if (tempBoard7Core.enabled == 0) {
                              
                                tempBoard7Core.selected = e.target;
                                tempBoard7Core.isFrame = true;
                                globalIframePath7Core = frameDocument;
                            }
                        }
                    }
                
            
            });


        }
    }
}

/*
------------- click/right click events -----------------------------
*/
$(document).mousedown(function (e) {
   
    if (e.which == 1) {
        if ($(e.target).is("textarea") || $(e.target).prop("type") == "email" || $(e.target).prop("type") == "text" || $(e.target).prop("type") == "password" || $(e.target).prop("type")==undefined && ($(e.target).prop("tagName") === "INPUT") ) {

            if (tempBoard7Core.enabled == 0) {
               
                tempBoard7Core.selected = e.target;
                if($(e.target).is("textarea")){
                            
                                tempBoard7Core.triggerClick("leftClick","TextArea");
                            }
                            else if($(e.target).attr("type") == "text"){
                                tempBoard7Core.triggerClick("leftClick","TextField");
                            }
                            else if($(e.target).attr("type") == "password"){
                                tempBoard7Core.triggerClick("leftClick","PasswordField");
                            }
                            else if($(e.target).attr("type")==undefined && (e.target.tagName=="INPUT")){
                                 tempBoard7Core.triggerClick("leftClick","TextField");
                            }
                 }


        }

    }
    else if (e.which == 3) {
		//alert(e.target);
		
        if ($(e.target).is("textarea") || $(e.target).prop("type") == "email"  || $(e.target).prop("type") == "text" || $(e.target).prop("type") == "password" || ($(e.target).prop("type")==undefined && $(e.target).prop("tagName")=="INPUT")) {
			
            if (tempBoard7Core.enabled == 0) {
              
                tempBoard7Core.selected = e.target;
            }
        }


    }
});

//-----------------------end of right click events-------------------


